import { Component, OnInit } from '@angular/core';
import * as confetti from 'canvas-confetti';
import { MatDialog } from '@angular/material/dialog';
import { PlayerComponent } from '../galeria/player/player.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  aberto: boolean = false;
  tempo: boolean = false;

  constructor(
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  abrirPresente() {
    if (!this.aberto) {
      confetti.create()({
        shapes: ['circle', 'square'],
        particleCount: 500,
        spread: 150,
        ticks: 300,
        origin: {
          y: (2.5),
          x: (0.5)
        }
      });
    }
    this.aberto = true
    
    setTimeout(() => {
      this.tempo = true;
  }, 118000);
  }
}
