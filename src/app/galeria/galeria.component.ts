import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PlayerComponent } from './player/player.component';

@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.component.html',
  styleUrls: ['./galeria.component.scss']
})
export class GaleriaComponent implements OnInit {

  videos: any[] = [
    { url: "assets/videos/mila_ze.mp4", thumb: "assets/videos/thumbs/1.png" },
    { url: "assets/videos/jonathan.mp4", thumb: "assets/videos/thumbs/2.png" },
    { url: "assets/videos/tio.mp4", thumb: "assets/videos/thumbs/3.png" },
    { url: "assets/videos/rapha.MP4", thumb: "assets/videos/thumbs/4.png" },
    { url: "assets/videos/vanessa.mp4", thumb: "assets/videos/thumbs/1.png" },
    { url: "assets/videos/bebella.mp4", thumb: "assets/videos/thumbs/2.png" },
    { url: "assets/videos/gu.mp4", thumb: "assets/videos/thumbs/3.png" },
    { url: "assets/videos/familia.mp4", thumb: "assets/videos/thumbs/4.png" },
    { url: "assets/videos/vil.mp4", thumb: "assets/videos/thumbs/1.png" },
    { url: "assets/videos/arthur.mp4", thumb: "assets/videos/thumbs/2.png" },
    { url: "assets/videos/glauber.mp4", thumb: "assets/videos/thumbs/4.png" },
    { url: "assets/videos/joyce.mp4", thumb: "assets/videos/thumbs/1.png" },
    { url: "assets/videos/silvia_gui.mp4", thumb: "assets/videos/thumbs/2.png" },
    { url: "assets/videos/cassiano.mp4", thumb: "assets/videos/thumbs/3.png" },
    { url: "assets/videos/bs.mp4", thumb: "assets/videos/thumbs/4.png" },
    { url: "assets/videos/sorin.mp4", thumb: "assets/videos/thumbs/1.png" },
    { url: "assets/videos/jeska.mp4", thumb: "assets/videos/thumbs/4.png" },
    { url: "assets/videos/ale.mp4", thumb: "assets/videos/thumbs/1.png" },
    { url: "assets/videos/facul.mp4", thumb: "assets/videos/thumbs/2.png" },
    { url: "assets/videos/papi_mami.mp4", thumb: "assets/videos/thumbs/3.png" },

  ];

  constructor(
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  openPlayer(url: string): void {
    const dialogRef = this.dialog.open(PlayerComponent, {
      data: { url: url },
      // maxHeight: '98vh'
    });
  }

}
